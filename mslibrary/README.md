## MS

使用此包可以轻松地将各种时间格式转换为毫秒。

本库由 [坚果](https://blog.csdn.net/qq_39132095),完成迁移。

## 一、下载安装

```
ohpm install @nutpi/ms
```

OpenHarmony ohpm
环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 二、使用

```js
import ms, { format, parse, StringValue } from '@nutpi/ms';

//https://github.com/vercel/ms


ms('2 days') // 172800000
ms('1d') // 86400000
ms('10h') // 36000000
ms('2.5 hrs') // 9000000
ms('2h') // 7200000
ms('1m') // 60000
ms('5s') // 5000
ms('1y') // 31557600000
ms('100') // 100
ms('-3 days') // -259200000
ms('-1h') // -3600000
ms('-200') // -200
```

### Convert from Milliseconds

```
ms(60000)             // "1m"
ms(2 * 60000)         // "2m"
ms(-3 * 60000)        // "-3m"
ms(ms('10 hours'))    // "10h"
```

### Time Format Written-Out

```
ms(60000, { long: true })             // "1 minute"
ms(2 * 60000, { long: true })         // "2 minutes"
ms(-3 * 60000, { long: true })        // "-3 minutes"
ms(ms('10 hours'), { long: true })    // "10 hours"
```

## 三、开源协议

本项目基于 [MIT](LICENSE) ，请自由地享受和参与开源。[感谢](https://github.com/jfromaniello/url-join)
坚果派的小伙伴做出的努力。[源库](https://github.com/vercel/ms)

## 四、运行环境

DevEco Studio 5.0 Release
Build Version: 5.0.3.906,

适用于API：12及以上，在真机Mate60测试ok。



